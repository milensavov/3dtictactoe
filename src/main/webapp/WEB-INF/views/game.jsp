<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <title>3D TicTacToe</title>
    <!--- link to the last version of babylon --->
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
    
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="resources/js/babylon.custom.js"></script>
    <script src="resources/js/game.js"></script>
    <style>
        html, body {
            overflow: hidden;
            width   : 100%;
            height  : 100%;
            margin  : 0;
            padding : 0;
        }

        #renderCanvas {
            width   : 100%;
            height  : 100%;
            touch-action: none;
        }
    </style>
</head>
<body>
<div style="float:left;width:90%;height:100%;">
    <canvas id="renderCanvas"></canvas>
        </div>
    <div style="float:left;width:10%">
    
	<a href="newgame?level=easy">New Easy Game</a><br/>
	<a href="newgame?level=hard">New Hard Game</a><br/>
	<a role="button" onclick="window.undo();">Undo move</a>
	
	<br/>
	<form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
	<a role="button" onclick="document.forms['logoutForm'].submit()">Log out</a>
    </div>
</body>
</html>