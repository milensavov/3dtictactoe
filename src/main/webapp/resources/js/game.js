        /* global BABYLON */
var undo;
window.addEventListener('DOMContentLoaded', function(){
           
            var initialState = {
/*                human: {0:"0:0:0",1:"0:1:2",2:"0:1:3",3:"3:3:3"},
                computer: {0:"2:2:1",1:"2:3:2",2:"1:2:1"}
*/            };


            $.ajax({ 
                url:"loadgame", 
                type:'get',    
                async: false,
                success: function (data) {                    
                    initialState = data;
                    console.log(initialState);
                },
                error : function(data) {
                    console.log("Error" + data)  
                }
            });
            
            // get the canvas DOM element
            var canvas = document.getElementById('renderCanvas');

            // load the 3D engine
            var engine = new BABYLON.Engine(canvas, true);
            
            var boxarr;
            var materialBox;
            var materialBoxSelectedHuman;
            var materialBoxSelectedComputer;
            // createScene function that creates and return the scene
            var createScene = function () {
                var scene = new BABYLON.Scene(engine);
            
                var camera = new BABYLON.ArcRotateCamera("Camera", 1, 1, 1, new BABYLON.Vector3(0, 0, 0), scene);
                camera.attachControl(canvas, true);
                camera.lowerBetaLimit = 0.1;
                camera.upperBetaLimit = (Math.PI / 2) * 0.99;
                camera.lowerRadiusLimit = 40;
                var light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0), scene);
                light.state="on";
                                           
                materialBox = new BABYLON.StandardMaterial("texture1", scene);
                materialBox.diffuseColor = new BABYLON.Color3(0, 1, 0);//Green
                materialBox.alpha = 0.25;
              
                materialBoxSelectedHuman = new BABYLON.StandardMaterial("texture1", scene);
                materialBoxSelectedHuman.diffuseColor = new BABYLON.Color3(1, 0, 0);//Red
                materialBoxSelectedHuman.alpha = 0.25;
                
                materialBoxSelectedComputer = new BABYLON.StandardMaterial("texture1", scene);
                materialBoxSelectedComputer.diffuseColor = new BABYLON.Color3(0, 0, 1);//Blue
                materialBoxSelectedComputer.alpha = 0.25;
                            
                boxarr=new Array();
                boxarr[0] = new Array();
                boxarr[1] = new Array();
                boxarr[2] = new Array();
                for( x=0; x<4;x++) {
                    boxarr[x] = new Array();
                    for( y=0; y<4;y++) {
                	boxarr[x][y] = new Array();
                            for( z=0; z<4;z++) {                        	
                        	//console.log(x+":"+y+":"+z);
                        	var b = null;
                        	b = BABYLON.Mesh.CreateBox(""+x+":"+y+":"+z, 1.8, scene,true);                                
                        	b.position.x=x+x*3;
                        	b.position.y=y+y*3;
                        	b.position.z=z+z*3;
                            b.clicked = false;
                        	b.material = materialBox;
                            
                                for(a in initialState.human) {
                                    if (b.id == initialState.human[a]) {
                                        b.material = materialBoxSelectedHuman;
                                        b.clicked = true;
                                    }
                                }
                                for(a in initialState.computer) {
                                    if (b.id == initialState.computer[a]) {
                                        b.material = materialBoxSelectedComputer;
                                        b.clicked = true;
                                    }
                                }
                            
                                var boxClickedAction = new BABYLON.ExecuteCodeAction(
                                    BABYLON.ActionManager.OnPickTrigger,
                                    function(evt) {
                                         var meshClicked = evt.meshUnderPointer;
                                         if (meshClicked.clicked == false ) {
                                            meshClicked.clicked = true;
                                        $.ajax({ 
                                        url:"statusupdate", 
                                        type:'get', 
                                        data: 'clicked='+meshClicked.id,
                                        success: function (data) {                                            
                                            initialState = data;
                                            console.log(initialState);
                                            redrawScene(boxarr,materialBox,materialBoxSelectedHuman,materialBoxSelectedComputer);
                                         },
                                         error : function(data) {
                                               initialState = {}
                                            	console.log("Error:"+data);                                                                                     
                                         }
                                         });
                                         meshClicked.material = materialBoxSelectedHuman;
                                       }
                                }
                            );
                            b.actionManager = new BABYLON.ActionManager(scene);
                            b.actionManager.registerAction                 
                            (new BABYLON.CombineAction(BABYLON.ActionManager.OnPickTrigger, [ // Then is used to add a child action used alternatively with the root action.                            
                            boxClickedAction,                                                // First click: root action. Second click: child action. Third click: going back to root action and so on...                                                           
                        ]));                        
                        boxarr[x][y].push(b)                                                
                        }
                    }
                }
                return scene;
            }


            // call the createScene function
            var scene = createScene();
            
            var redrawScene = function(boxarr,matarialBox,materialBoxSelectedHuman,materialBoxSelectedComputer) {                   
            	for( x=0; x<4;x++) {                
                	for( y=0; y<4;y++) {                
                            for( z=0; z<4;z++) {
                            	b = boxarr[x][y][z];                      
                            	
                            	b.material = materialBox;
                            	b.clicked = false;
                            	
                            	if (initialState.winner != undefined) {
                            		b.clicked = true;
                            	}
                            	
                                for(a in initialState.human) {                                  		
                                    if (b.id === initialState.human[a]) {                                        
                                        b.material = materialBoxSelectedHuman;
                                        b.clicked = true;
                                    }
                                }
                                for(a in initialState.computer) {                                	
                                    if (b.id === initialState.computer[a]) {                                       
                                        b.material = materialBoxSelectedComputer;
                                        b.clicked = true;
                                    }
                                }
                        }
                    }                	
                } 
                if (initialState.winner != undefined) {
                	if (initialState.winner[0] == "1000:1000:1000")
                		alert("The winner is COMPUTER!");
                	else 
                		alert("The winner is HUMAN!");
                }
            }

            // run the render loop
            engine.runRenderLoop(function(){
                scene.render();
            });

            // the canvas/window resize event handler
            window.addEventListener('resize', function(){
                engine.resize();
            });

            undo = function() {
                $.ajax({ 
                    url:"undo", 
                    type:'get',    
                    async: false,
                    success: function (data) {                    
                        initialState = data;
                        console.log(initialState);
                        redrawScene(
                        		boxarr, materialBox, materialBoxSelectedHuman, 
                        		materialBoxSelectedComputer);
                    },
                    error : function(data) {
                        console.log("Error" + data)  
                   }
                });            	
            }            

});
