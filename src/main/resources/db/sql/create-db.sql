
CREATE TABLE user (
  id int NOT NULL  ,
  username varchar(255) ,
  password varchar(255),
  game varchar(10000)  ,
  PRIMARY KEY (id)
);

CREATE TABLE saved_games (
  id int NOT NULL  ,
  user_id int NOT NULL,
  game varchar(10000),
  PRIMARY KEY (id,user_id)
) ;
  CREATE TABLE role (
  id int NOT NULL ,
  name varchar(45),
  PRIMARY KEY (id)
)
CREATE TABLE user_role (
  user_id int NOT NULL,
  role_id int NOT NULL,
  PRIMARY KEY (user_id,role_id)
   )