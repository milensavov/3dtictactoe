package tictactoe.game;

import tictactoe.game.model.Point3D;

public interface Player {
	public static char EMPTY_MARK = '_';
	Point3D play(Point3D p);
}
