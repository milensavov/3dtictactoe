package tictactoe.game.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

import tictactoe.game.model.Point3D;

public class Point3DDTO {

	@JsonIgnore
	private Point3D point;
	
	
	private String data;
	
	@JsonValue
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Point3DDTO(Point3D p) {
		this.data = p.getX()+":"+p.getY()+":"+p.getZ();
	}
	
	
}
