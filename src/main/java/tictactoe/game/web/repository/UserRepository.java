package tictactoe.game.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tictactoe.game.web.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
