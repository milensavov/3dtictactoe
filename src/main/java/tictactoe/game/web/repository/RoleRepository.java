package tictactoe.game.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tictactoe.game.web.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
