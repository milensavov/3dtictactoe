package tictactoe.game.web;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tictactoe.game.Computer;
import tictactoe.game.Game;
import tictactoe.game.Human;
import tictactoe.game.Game.GAME_STRATEGY;
import tictactoe.game.model.Box;
import tictactoe.game.model.Cube;
import tictactoe.game.model.Point3D;
import tictactoe.game.web.model.Point3DDTO;
import tictactoe.game.web.service.UserService; 

@Controller
public class GameController {

    @Autowired
    private UserService userService;

	@Autowired()
	private Game game;
	
	 
	public void setGame(Game game) {
		this.game = game;
	}

	@RequestMapping("/newgame")
	public String newGame(@RequestParam(value="level",required=false) String level,
			Authentication authentication) {
		if (authentication == null) {
			return "redirect:/login";
		}
		GAME_STRATEGY strategy;
		if (level != null && level.equalsIgnoreCase("easy")) {
			strategy = Game.GAME_STRATEGY.WEAK;
		}
		else {
			strategy = Game.GAME_STRATEGY.FULL;
		}
		
		Cube cube = game.newGame(strategy);
		UserDetails activeUser = (UserDetails) authentication.getPrincipal();
		tictactoe.game.web.model.User user = new tictactoe.game.web.model.User();
		try {
			user.setGame(toString(cube.getCube()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		user.setUsername(activeUser.getUsername());		
		userService.updateGame(user);
		
		return "redirect:/game";
	}

	@RequestMapping("/loadgame")
	@ResponseBody
	public  Map<String,List<Point3DDTO>> loadGame(Authentication authentication) {
		Map<String,List<Point3DDTO>> result = new HashMap<>();
		
		UserDetails activeUser = (UserDetails) authentication.getPrincipal();
		Cube cube = loadGameFromDB(activeUser);
		result = cubeToMarkedBoxes(cube, result);
		return result;
	}
	
	@RequestMapping(value="/statusupdate",produces = "application/json")
	@ResponseBody
	public Map<String,List<Point3DDTO>> statusUpdate(
			@RequestParam(value="clicked", required=false) String clicked, Authentication authentication) {
		Map<String,List<Point3DDTO>> result = new HashMap<>();
		Cube cube = null;
		if (clicked != null && clicked.isEmpty() == false ) {
			game.play(clicked);
		}	
		cube = game.getStatus();
		result = cubeToMarkedBoxes(cube, result);
		UserDetails activeUser = (UserDetails) authentication.getPrincipal();
		saveGameToDB(activeUser,cube);
		return result;
	}

	
	@RequestMapping("game")
	public String game() {
		return "game";
	}

	@RequestMapping(value="/undo",produces = "application/json")
	@ResponseBody
	public Map<String,List<Point3DDTO>> undo(Authentication authentication) {
		Map<String,List<Point3DDTO>> result = new HashMap<>();
		Cube cube = game.undo();
		UserDetails activeUser = (UserDetails) authentication.getPrincipal();
		saveGameToDB(activeUser, cube);
		result = cubeToMarkedBoxes(cube, result);
		return result;
	}
	
	private Map<String,List<Point3DDTO>> cubeToMarkedBoxes(Cube cube,Map<String,List<Point3DDTO>> result) {
		Map<Character,List<Point3D>> boxMap = cube.getAllMarkedBoxes();
		result.put("computer", new ArrayList<>());
		result.put("human", new ArrayList<>());
		int cubeValue = cube.evaluate();
		List<Point3D> marks = boxMap.get(Human.MARK);
		for (Point3D p : marks) {
			result.get("human").add(new Point3DDTO(p));
		}
		
		marks = boxMap.get(Computer.MARK);
		for (Point3D p : marks) {
			result.get("computer").add(new Point3DDTO(p));
		}
		
		if (cubeValue== 1000 || cubeValue == -1000) {
			List<Point3DDTO> winner = new ArrayList<>();
			winner.add(new Point3DDTO(new Point3D(cubeValue,cubeValue,cubeValue)));
			result.put("winner", winner);
		}
		return result;
	}

	/**
	 * Serialize object
	 * @param o
	 * @return
	 * @throws IOException
	 */
    private static String toString( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray()); 
    }
    
    /**
     * Unserialize object
     * @param s
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static Object fromString( String s ) throws IOException , ClassNotFoundException {
		byte [] data = Base64.getDecoder().decode( s );
		ObjectInputStream ois = new ObjectInputStream( 
		new ByteArrayInputStream(  data ) );
		Object o  = ois.readObject();
		ois.close();
		return o;
	}
    
	private Cube loadGameFromDB(UserDetails activeUser) {
		tictactoe.game.web.model.User user = userService.findByUsername(activeUser.getUsername());
		String game = user.getGame();
		Cube cube = new Cube(4);
		try {
			cube = new Cube((Box[][][])fromString(game));
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return cube;
	}
	
	private void saveGameToDB(UserDetails activeUser,Cube cube) {

		tictactoe.game.web.model.User user = new tictactoe.game.web.model.User();
		try {
			user.setGame(toString(cube.getCube()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		user.setUsername(activeUser.getUsername());		
		userService.updateGame(user);
			
	}
}
