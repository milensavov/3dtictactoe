package tictactoe.game.web.service;

import tictactoe.game.web.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
    
	void updateGame(User user);
}
