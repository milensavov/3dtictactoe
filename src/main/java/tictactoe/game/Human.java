package tictactoe.game;

import java.util.Stack;

import tictactoe.game.model.Cube;
import tictactoe.game.model.Point3D;

public class Human implements Player {

	public static final char MARK = 'X'; 
	private Cube cube;
	private Stack<Point3D> history;
	
	public Human (Cube cube,Stack<Point3D> history) {
		this.cube = cube;
		this.history=history;
	}
	
	@Override
	public Point3D play(Point3D p) {
		if (cube.isFree(p) == true) {
			cube.setBoxStatus(p, Human.MARK);
			return p;
		}
		else {
			//System.out.println("try again");
			//throw new RuntimeException("Not empty. Try again");
			return null;
		}
	}
	
	

}
