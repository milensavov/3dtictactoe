package tictactoe.game;

import java.util.Scanner;
import java.util.Stack;

import org.springframework.stereotype.Component;

import tictactoe.game.ai.MinMax;
import tictactoe.game.ai.MinMaxAB;
import tictactoe.game.ai.MinMaxWeak;
import tictactoe.game.model.Cube;
import tictactoe.game.model.Point3D;

@Component
public class Game {

	public final static int GAME_SIZE= 4;
	private final Cube cube;
	private final Player human;
	//TODO : think this over: how is it better to change the strategy of the computer player!
	private Player computer;
	private final Stack<Point3D> history;
	private MinMax minMaxStrategy;
	public enum GAME_STRATEGY {WEAK,FULL};
	
	public static void main(String[] args) {
		Game game = new Game();
		game.play();
	}
	
	
	public Game() {
		setStrategy(GAME_STRATEGY.FULL);
		history = new Stack<>();
		cube = new Cube(GAME_SIZE);
		human = new Human(cube,history);
		computer = new Computer(cube,history,this.minMaxStrategy);		
	}

	public Game(Cube cube,GAME_STRATEGY strategy) {
		this.cube = cube;
		setStrategy(strategy);
		history = new Stack<>();
		human = new Human(cube,history);
		computer = new Computer(cube,history,minMaxStrategy);	
	}

	private void setStrategy(GAME_STRATEGY strategy) {
		switch (strategy) {
		case WEAK:
			this.minMaxStrategy = new MinMaxWeak();
			break;
		case FULL:
			this.minMaxStrategy = new MinMaxAB();
		break;
		}
		this.computer = new Computer(cube,history,minMaxStrategy);
	}
	
	public Game(int cubeSize, Player human, Player computer) {
		this.cube = new Cube(cubeSize);
		this.human = human;
		this.computer = computer;
		history = new Stack<>();
	}

	/**
	 * Play the console version of the game
	 */
	public void play() {		
		int result = 0;
		do {			
			while (human.play(humanMove()) == null) {
				System.out.println("Waiting for valid input");
			}
			System.out.println(cube);
			result = cube.evaluate();			
			if (result == 1000) {
				System.out.println("Game OVER! Winner is HUMAN! "  + result   );
				break;
			}
			computer.play(null);
			System.out.println(cube);
			result = cube.evaluate();
			if (result == -1000) {
				System.out.println("Game OVER! Winner is COMPUTER! "  + result   );
				break;
			}			
			System.out.println("Cube status: "  + result);
		}	while (result != 1000 || result != -1000);	
	}
	
	/**
	 * Play a human move and respond
	 * @param data string input of X, Y and Z separated by colon : X:Y:Z
	 * @return cube status
	 */
	public Cube play(String data) {
		int x,y,z;
		x=y=z=0;
		try {
			String[] coords = data.split(":");
			if (coords.length != 3 ) {
				throw new RuntimeException("Invalid coordinates");
			}
			x = Integer.parseInt(coords[0]);
			y = Integer.parseInt(coords[1]);
			z = Integer.parseInt(coords[2]);
		}
		catch (NumberFormatException e) {
			throw new RuntimeException("Invalid coordinates");
		}
		catch (NullPointerException e) {
			throw new RuntimeException("Invalid coordinates");
		}
		Point3D move = new Point3D(x,y,z);
		history.push(human.play(move));
		history.push(computer.play(null));
		//System.out.println(cube);
		return cube;
	}
	
	public Cube getStatus() {
		return cube;
	}
	
	private Point3D humanMove() {
		Scanner s = new Scanner(System.in);
		System.out.println("C'moon");
		int x = s.nextInt();
		int y = s.nextInt();
		int z = s.nextInt();
		Point3D p = new Point3D(x,y,z);	
		s.close();
		return p;
	}


	public Cube newGame(GAME_STRATEGY strategy) {
		history.clear();
		cube.clean();
		setStrategy(strategy);
		return cube;
	}

	/**
	 * Undoes the last 2 moves: computer's and human's
	 * @return cube status
	 */
	public Cube undo() {
		if (history.size() >= 2) {
			cube.setFree(history.pop()); // Computer's move
			cube.setFree(history.pop()); // Human's move
		}
		return cube;
	}
	
}
