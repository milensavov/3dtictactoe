package tictactoe.game;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

import tictactoe.game.ai.MinMax;
import tictactoe.game.ai.MinMaxAB;
import tictactoe.game.ai.MinMaxWeak;
import tictactoe.game.model.Cube;
import tictactoe.game.model.Point3D;


public class Computer  implements Player {

	public static final char MARK = 'O'; 
	private static int itcount = 0;
	private Cube cube;
	private MinMax minMaxStrategy ;
	Stack<Point3D> history;
	
	
	public void setMinMaxStrategy(MinMax minMaxStrategy) {
		this.minMaxStrategy = minMaxStrategy;
	}

	/**
	 * 
	 * @param cube
	 * @param minMaxStrategy MinMaxWeak algorithm makes mistakes and can be beaten. 
	 * MinMaxAB play the perfect game
	 */
	public Computer(Cube cube,Stack<Point3D> history, MinMax minMaxStrategy) {
		this.cube = cube;
		this.history= history;
		this.minMaxStrategy = minMaxStrategy;
	}
	
	
	@Override
	public Point3D play(Point3D p) {
		Point3D coords = getNextMove(cube);
		cube.setBoxStatus(coords, MARK);
		return coords;
	}
	
	private Point3D getNextMove(Cube cube) {
		Point3D bestMove = new Point3D(-1,-1,-1);
		int bestScore = Integer.MIN_VALUE;
		List<Point3D> availableMoves = cube.getAvailableMoves();
//		Collections.shuffle(availableMoves);
		itcount=0;
		for (Point3D toTest : availableMoves) {			
			cube.setBoxStatus(toTest, Computer.MARK); 
			//int currentScore = minMax(2, Integer.MIN_VALUE, Integer.MAX_VALUE,false);
			//int currentScore = minMaxAB(2, Integer.MIN_VALUE, Integer.MAX_VALUE,false);
			//int currentScore = minMaxWeak(2,Human.MARK, Integer.MIN_VALUE, Integer.MAX_VALUE);
			int currentScore = minMaxStrategy.minMax(cube, 2, Integer.MIN_VALUE, Integer.MAX_VALUE, Human.MARK);
			cube.setFree(toTest);
			System.out.println(toTest + " value added " + currentScore);
			if (currentScore > bestScore) {
				bestMove = toTest;
				bestScore = currentScore;
			}			
		}	
		System.out.println("Picked move" + bestMove + " Score "+ bestScore + " in "+ itcount);
		return bestMove;
	}

	private int minMaxAB(int depth, int a, int b, boolean isMaximizer) {					
		List<Point3D> availableMoves = cube.getAvailableMoves();
		//Collections.shuffle(availableMoves);
		itcount++;
		//int bestScore = isMaximizer ? Integer.MIN_VALUE : Integer.MAX_VALUE;
		int currentScore = cube.evaluate();
		if (availableMoves.isEmpty()  || depth == 0) {
			return currentScore;
		}
		else {	
			if (!isMaximizer) {	
				for (Point3D toTest : availableMoves) {
						cube.setBoxStatus(toTest, Human.MARK);
						//System.out.println("minimizer: " +  itcount + " @depth "  + depth);		
												
						currentScore = minMaxAB(depth-1,a,b, !isMaximizer);
						cube.setFree(toTest);
						if (currentScore < b) {
							b = currentScore;											
						}
						b = b-(depth);												
						if (a >= b) 
							break;
				}
				return b;
				//return bestScore;
			}
			else {
				for (Point3D toTest : availableMoves) {
						cube.setBoxStatus(toTest, Computer.MARK);					
						
						//System.out.println("maximizer: "+itcount + " @depth "  + depth);
						
						currentScore = minMaxAB(depth-1,a,b, !isMaximizer);
						cube.setFree(toTest);
						if (currentScore > a) {
							a = currentScore;														
						}						
						a = a+(depth);																		
						if (a >= b) 
							break;						
				}
				return a;
				//return bestScore;
			}
		}
	}

   private int minMaxWeak(int depth, char player, int alpha, int beta) {		    
	      List<Point3D> nextMoves = cube.getAvailableMoves();
	      
	      // Computer O is maximizing; while Human X is minimizing
	      int score;		     
	      itcount++;
	      if (nextMoves.isEmpty() || depth == 0) {
	         score = cube.evaluate();
	         return score;

	      } else {
	         for (Point3D move : nextMoves) {	            
	        	cube.setBoxStatus(move, player);
	            if (player == Computer.MARK) {  // O is the maximizing player
	               score = minMaxWeak(depth - 1, Human.MARK, alpha, beta);
	               if (score > alpha) {
	                  alpha = score;
	               }
	            } else {  // X is the minimizing player (Human)
	               score = minMaxWeak(depth - 1, Computer.MARK, alpha, beta);
	               if (score < beta) {
	                  beta = score;
	               }
	            }
	            // undo move
	            cube.setFree(move);
	            // cut-off
	            if (alpha >= beta) break;
	         }
	         int ret = (player == Computer.MARK) ? alpha : beta;
	         return ret;
	      }
	   }


	private int minMax(int depth, int a, int b, boolean isMaximizer) {
		int currentScore = cube.evaluate();		
		itcount++;
		if (depth == 0) {
			//System.out.println("Current score : "+ currentScore);
			return currentScore;
		}
		if (cube.hasMoreMoves() == false) {
			System.out.println("hasMoreMoves false "+0);
			return currentScore;
		}
		
		if (!isMaximizer) {
			int bestScore = Integer.MAX_VALUE;
			List<Point3D> availableMoves = cube.getAvailableMoves();
			for (Point3D toTest : availableMoves) {
						if (cube.isFree(toTest)) {
							cube.setBoxStatus(toTest, Human.MARK);							
							bestScore = Math.min(bestScore,minMax(depth-1,a,b, !isMaximizer));
							bestScore = bestScore+(depth);
							a = Math.min(a, bestScore);
							cube.setFree(toTest);													
				}
			}	

			return bestScore;
		}
		else {
			int bestScore = Integer.MIN_VALUE;

			List<Point3D> availableMoves = cube.getAvailableMoves();
			for (Point3D toTest : availableMoves) {
						if (cube.isFree(toTest)) {
							cube.setBoxStatus(toTest, Computer.MARK);													
							bestScore = Math.max(bestScore,minMax(depth-1,a,b, !isMaximizer));
							bestScore = bestScore-(depth);
							b = Math.max(b,bestScore);
							cube.setFree(toTest);
				}
			}
			return bestScore;
		}
	}
	
}
