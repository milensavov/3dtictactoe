package tictactoe.game.ai;

import java.util.List;

import tictactoe.game.Computer;
import tictactoe.game.Human;
import tictactoe.game.model.Cube;
import tictactoe.game.model.Point3D;

public class MinMaxAB implements MinMax {

	@Override
	public int minMax(Cube cube, int depth, int a, int b, char player) {
		List<Point3D> availableMoves = cube.getAvailableMoves();

		int currentScore = cube.evaluate();
		if (availableMoves.isEmpty()  || depth == 0) {
			return currentScore;
		}
		else {	
			if (player == Human.MARK) {	
				for (Point3D toTest : availableMoves) {
						cube.setBoxStatus(toTest, Human.MARK);
						//System.out.println("minimizer: " +  itcount + " @depth "  + depth);		
												
						currentScore = minMax(cube,depth-1,a,b, Computer.MARK);
						cube.setFree(toTest);
						if (currentScore < b) {
							b = currentScore;											
						}
						b = b-(depth);												
						if (a >= b) 
							break;
				}
				return b;
				
			}
			else {
				for (Point3D toTest : availableMoves) {
						cube.setBoxStatus(toTest, Computer.MARK);					
						
						//System.out.println("maximizer: "+itcount + " @depth "  + depth);
						
						currentScore = minMax(cube,depth-1,a,b, Human.MARK);
						cube.setFree(toTest);
						if (currentScore > a) {
							a = currentScore;														
						}						
						a = a+(depth);																		
						if (a >= b) 
							break;						
				}
				return a;
				
			}
		}
	}

}
