package tictactoe.game.ai;

import java.util.List;

import tictactoe.game.Computer;
import tictactoe.game.Human;
import tictactoe.game.model.Cube;
import tictactoe.game.model.Point3D;

public class MinMaxWeak implements MinMax {

	@Override
	public int minMax(Cube cube, int depth, int alpha, int beta, char player) {
	      List<Point3D> nextMoves = cube.getAvailableMoves();
	      
	      // Computer O is maximizing; while Human X is minimizing
	      int score;		     
	      
	      if (nextMoves.isEmpty() || depth == 0) {
	         score = cube.evaluate();
	         return score;

	      } else {
	         for (Point3D move : nextMoves) {	            
	        	cube.setBoxStatus(move, player);
	            if (player == Computer.MARK) {  // O is the maximizing player
	               score = minMax(cube,depth - 1, alpha, beta, Human.MARK);
	               if (score > alpha) {
	                  alpha = score;
	               }
	            } else {  // X is the minimizing player (Human)
	               score = minMax(cube,depth - 1, alpha, beta, Computer.MARK);
	               if (score < beta) {
	                  beta = score;
	               }
	            }
	            // undo move
	            cube.setFree(move);
	            // cut-off
	            if (alpha >= beta) break;
	         }
	         int ret = (player == Computer.MARK) ? alpha : beta;
	         return ret;
	      }

	}

}
