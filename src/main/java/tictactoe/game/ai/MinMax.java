package tictactoe.game.ai;

import tictactoe.game.model.Cube;

public interface MinMax {
	int minMax(Cube cube, int depth, int alpha, int beta, char player);
}
