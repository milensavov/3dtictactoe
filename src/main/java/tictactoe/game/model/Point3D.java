package tictactoe.game.model;

import java.util.List;

public class Point3D  {
	private int x;
	private int y;
	private int z;
	
	public Point3D(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}
	
	@Override
	public String toString() {
		return "["+x+", "+y+", "+z+"]";
	}	
	
	@Override
	public boolean equals(Object o) {
		if ( o== null || getClass() != o.getClass()) 
			return false;
		Point3D p2 = (Point3D) o;
		if (this.getX() == p2.getX() && this.getY() == p2.getY() && this.getZ() == p2.getZ())
			return true;
		return false;
	}
	
	public boolean inLine(Point3D p2) {
		if (this.getX() == p2.getX() && this.getY() == p2.getY() ||
				this.getZ() == p2.getZ() && this.getY() == p2.getY() ||
				this.getX() == p2.getX() && this.getZ() == p2.getZ() )
			return true;
		return false;
	}
	

	
	public boolean inSameLine(List<Point3D> line ) {
		for (Point3D point : line) {
			//if (this.vector == point.vector) ///
		}
		
	return false;
		/*	List<Integer> l =new ArrayList<Integer>(); 
		if (this.getX() == p2.getX() && this.getY() == p2.getY()) {
			l.add(p2.getX());
			l.add(p2.getY());
			return l;
		}
		if (this.getZ() == p2.getZ() && this.getY() == p2.getY())
		if (this.getX() == p2.getX() && this.getZ() == p2.getZ() )		
	*/	
	}

	
}
