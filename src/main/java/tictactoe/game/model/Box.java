package tictactoe.game.model;

import java.io.Serializable;

public class Box implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private char mark;

	public Box(char mark) {
		this.mark = mark;
	}
	
	public void setMark(char mark) {
		this.mark = mark;
	}
	
	public char getMark() {
		return mark;
	}
	
	
}
