package tictactoe.game.model;

public class Pair {

	private int a;
	private int b;
	private int c;
	public int getA() {
		return a;
	}
	public int getB() {
		return b;
	}
	public int getC() {
		return c;
	}
	public Pair(int a, int b, int c) {
		
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	@Override
	public int hashCode() {
		return a*100+b*10+c;
	}
	
	@Override
	public String toString() {
		return "["+a+":"+b+":"+c+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) return false;
		Pair p2 = (Pair)obj;
		return a==p2.getA() && b==p2.getB() && c==p2.getC();
	}
	
}
