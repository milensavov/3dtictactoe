package tictactoe.game.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tictactoe.game.Computer;
import tictactoe.game.Human;
import tictactoe.game.Player;

public class Cube implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final int CUBE_SIZE;
	
	private Box[][][] cube;
	
	public Cube(int size) {
		CUBE_SIZE = size;
		cube = new Box[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];
		for (int x = 0; x < CUBE_SIZE; x++) {
			for (int y = 0; y < CUBE_SIZE; y++) {
				for (int z = 0; z < CUBE_SIZE; z++) {
					cube[x][y][z] = new Box(Player.EMPTY_MARK);
				}
			}
		}
	}
	
	public Cube(Box[][][] cube,int size) {
		CUBE_SIZE = size;
		this.cube = cube;
	}

	public Box[][][] getCube() {
		return cube;
	}

	public Cube(Box[][][] cube) {
		CUBE_SIZE = cube[0][0].length;
		this.cube = cube;
	}
	
	/**
	 * Marks a box as used
	 * @param coords
	 * @param mark
	 */
	public void setBoxStatus(Point3D coords, char mark) {
		int x = coords.getX();
		int y = coords.getY();
		int z = coords.getZ();
		if (0<=x && x <CUBE_SIZE && 0<=z && y<CUBE_SIZE && 0<=z && z<CUBE_SIZE) {
			cube[x][y][z].setMark(mark);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder buff = new StringBuilder("");
		for (int x = 0; x < CUBE_SIZE; x++) {
			buff.append("\t{\nX=" + x);
			for (int y = 0; y < CUBE_SIZE; y++) {
				buff.append("Y="+ y + "\t\t{ " );
				for (int z = 0; z < CUBE_SIZE; z++) {
					//buff.append("Z="+z+ ": ");
					buff.append(String.valueOf(cube[x][y][z].getMark()));
					buff.append(", ");
				}
				buff.append("}\n");
			}
			buff.append("\t}\n");
		}
		return buff.toString();
	}
	
	/**
	 * Returns true if the point is free
	 * @param point
	 * @return
	 */
	public boolean isFree(Point3D point) {
		return cube[point.getX()][point.getY()][point.getZ()].getMark() == Player.EMPTY_MARK;
	}

	/**
	 * Returns the mark of the box
	 * @param point
	 * @return
	 */
	public char getMark(Point3D point) {
		return cube[point.getX()][point.getY()][point.getZ()].getMark();
	}
	
	/**
	 * Sets the box empty
	 * @param point
	 */
	public void setFree(Point3D point) {
		cube[point.getX()][point.getY()][point.getZ()].setMark(Player.EMPTY_MARK);
	}
	
	/**
	 * Resets the cube
	 */
	public void clean() {
		for (int x = 0; x < CUBE_SIZE; x++) {
			for (int y = 0; y < CUBE_SIZE; y++) {
				for (int z = 0; z < CUBE_SIZE; z++) {
					setFree(new Point3D(x, y, z));
				}
			}
		}		
	}
	
	/**
	 * Returns true if there are any more free boxes
	 * @return
	 */
	public boolean hasMoreMoves() {
		boolean result = false;
		for (int x = 0; x < CUBE_SIZE; x++) {
			for (int y = 0; y < CUBE_SIZE; y++) {
				for (int z = 0; z < CUBE_SIZE; z++) {
					if (cube[x][y][z].getMark() == Player.EMPTY_MARK)
						result = true;
				}
			}
		}		
		return result;
	}

	/**
	 * Returns a map of the marked boxes
	 * @return
	 */
	public Map<Character,List<Point3D>> getAllMarkedBoxes() {
		Map<Character,List<Point3D>> result = new HashMap<>();
		result.put(Computer.MARK, new ArrayList<>());
		result.put(Human.MARK, new ArrayList<>());
		for (int x = 0; x < CUBE_SIZE; x++) {
			for (int y = 0; y < CUBE_SIZE; y++) {
				for (int z = 0; z < CUBE_SIZE; z++) {
					if (cube[x][y][z].getMark() == Human.MARK) {
						result.get(Human.MARK).add(new Point3D(x, y, z));
					}
					else if (cube[x][y][z].getMark() == Computer.MARK) {
						result.get(Computer.MARK).add(new Point3D(x, y, z));
					}
				}
			}
		}	
		return result;
	}
	
	/**
	 * Returns a list of all free boxes 
	 * @return 
	 */
	public List<Point3D> getAvailableMoves() {
		List<Point3D> ret = new ArrayList<>();
		for (int x = 0; x < CUBE_SIZE; x++) {
			for (int y = 0; y < CUBE_SIZE; y++) {
				for (int z = 0; z < CUBE_SIZE; z++) {
					if (cube[x][y][z].getMark() == Player.EMPTY_MARK)
						ret.add(new Point3D(x,y,z));
				}
			}
		}		
		return ret;
	}
	
	/**
	 * Returns the score of the cube;
	 * @return
	 */
	public int evaluate() {
		List<Point3D> humans = new ArrayList<>();
		List<Point3D> computers = new ArrayList<>();
		for (int x = 0; x < CUBE_SIZE; x++) {
			for (int y = 0; y < CUBE_SIZE; y++) {		
				for (int z = 0; z < CUBE_SIZE; z++) {
					if (cube[x][y][z].getMark() == Computer.MARK) {						
						computers.add(new Point3D(x,y,z));
					}
					else if (cube[x][y][z].getMark() == Human.MARK) {						
						humans.add(new Point3D(x,y,z));
					}					
				}				
			}
		}		
		// Check and return human result first in order to give him precedence in evaluation
		int ret = checkLines(humans, -1000);
		if (ret != 0) return ret;
		ret = checkDiagonals(humans, -1000);
		if (ret != 0) return ret;
		ret = checkLines(computers, 1000);
		if (ret != 0) return ret;
		ret = checkDiagonals(computers, 1000);
		if (ret != 0) return ret;
		
		return 0;
	}
	
	private int checkDiagonals(List<Point3D> list, int expectedReturn) {
		
		for (int z=0; z<CUBE_SIZE; z++) {  
		int count=0;
			for (int x=0,y=0; x<CUBE_SIZE&&y<CUBE_SIZE; x++,y++) {			
			//ok
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z) {
						count++;
					}
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
			}
		}	
		
		
		for (int x=0; x<CUBE_SIZE; x++) {
			int count=0;
			for (int z=0,y=0; z<CUBE_SIZE&&y<CUBE_SIZE; z++,y++) {
				for(Point3D p:list) {  //ok
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
			}
		}
		
		for (int x=0; x<CUBE_SIZE; x++) {
			int count=0;
			for (int z=0,y=CUBE_SIZE-1; z<CUBE_SIZE&&y>=0; z++,y--) {			
			//ok
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
			}
		}
		for (int z=0; z<CUBE_SIZE; z++) {
			int count=0;	
			for (int x=0,y=CUBE_SIZE-1; x<CUBE_SIZE&&y>=0; x++,y--) {
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++; 
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
			}
		}
		
		for (int y=0; y<CUBE_SIZE; y++) {
			int count=0;
			for (int x=0,z=0; x<CUBE_SIZE&&z<CUBE_SIZE; x++,z++) {		
				for(Point3D p:list) {  //ok
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
			}
		}
		
		for (int y=0; y<CUBE_SIZE; y++) {
			int count=0;
		for (int x=0,z=CUBE_SIZE-1; x<CUBE_SIZE&&z>=0; x++,z--) {
			
			 //ok
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
			}
		}
		
		
		//Cross diagonals
		int count=0;
		for (int x=0,y=0,z=CUBE_SIZE-1; x<CUBE_SIZE&&y<CUBE_SIZE&&z>=0; x++,y++,z--) {			
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
		}
		count=0;
		for (int y=0,z=CUBE_SIZE-1,x=CUBE_SIZE-1; y<CUBE_SIZE&&z>=0&&x>=0; y++,z--,x--) {			
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
		}
		count=0;
		for (int x=CUBE_SIZE-1,y=0,z=0; y<CUBE_SIZE&&z<CUBE_SIZE&&x>=0; y++,z++,x--) {			
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
		}
		
		count=0;
		for (int x=0,z=0,y=0; x<CUBE_SIZE&&z<CUBE_SIZE&&y<CUBE_SIZE; x++,z++,y++) {			
				for(Point3D p:list) {
					if(p.getX()==x && p.getY()==y &&p.getZ()==z)
						count++;
					if(count == CUBE_SIZE) 
						return expectedReturn;
				}
		}
		return 0;
	}
	
	private int checkLines(List<Point3D> list, int expectedReturn) {
		Map<Pair,Integer> map = new HashMap<>(10);
		for (int i=0; i<list.size(); i++) {
			Point3D p1 = list.get(i);
			for (int x=i+1; x < list.size(); x++) {
				Point3D p2 = list.get(x);
				if (p1.getX() == p2.getX() && p1.getY() == p2.getY() ) {
					Pair key = new Pair(p1.getX(),p1.getY(),-1);
					int value = map.get(key) == null ? 0 : map.get(key)+1;
					map.put(key, value);
					if (value == CUBE_SIZE) return expectedReturn;
				}
				if (p1.getX() == p2.getX() && p1.getZ() == p2.getZ() ) {
					Pair key = new Pair(p1.getX(),-1,p1.getZ());
					int value = map.get(key) == null ? 0 : map.get(key)+1;
					map.put(key, value);
					if (value == CUBE_SIZE) return expectedReturn;
				}
				if (p1.getZ() == p2.getZ() && p1.getY() == p2.getY() ) {
					Pair key = new Pair(-1,p1.getY(),p1.getZ());
					int value = map.get(key) == null ?  0 : map.get(key)+1;
					map.put(key, value);
					if (value == CUBE_SIZE) return expectedReturn;
				}
			}
		}
		return 0;
	}
	
/*	
	public static void main(String[] args) {
		Box[][][] box = initBox();
		Cube cube = new Cube(box,3);
		//System.out.println(cube);
		//System.out.println(cube.evaluate());
		Player human = new Human(cube);
		Player computer = new Computer(cube);
		int result = 0;
		do {
			human.play();
			System.out.println(cube);
			result = cube.evaluate();			
			if (result == 1000) {
				System.out.println("Game OVER! Winner is HUMAN! "  + result   );
				break;
			}
			computer.play();
			System.out.println(cube);
			result = cube.evaluate();
			if (result == -1000) {
				System.out.println("Game OVER! Winner is COMPUTER! "  + result   );
				break;
			}			
			System.out.println("Cube status: "  + result);
		}	while (result != 1000 || result != -1000);	
	}
*/	
	private static Box[][][] initBox() {
		Box _ = new Box(Player.EMPTY_MARK);
		Box x = new Box(Human.MARK);
		Box o = new Box(Computer.MARK);
		
		Box[][][] box = {
				// Z=0
				{ 
					{ x, o, _ },
					{ _, o, _ },
					{ _, o, _ }
				},
				//Z=1
				{
					{ _, _, _ },
					{ _, _, _ },
					{ _, _, _ }
				},
				//Z=2
				{
					{ _, _, _ },
					{ _, _, _ },
					{ _, _, _ }
				}
		};
		return box;
		/*Box[][][] box = {
			// Z=0
			{ 
				{ x,_, _, _ },
				{ _, _, _, _ },
				{ _, o, _, _ },
				{ _, _, _, _ }
			},
			//Z=1
			{
				{ _, _, _, _ },
				{ _, x, o, _ },
				{ _, o, _, _ },
				{ _, _, _, _ }
			},
			//Z=2
			{
				{ _, _, _, _ },
				{ _, _, _, _ },
				{ _, o, _, _ },
				{ _, _, _, _ }
			},
			//Z=3
			{
				{ _, _, _, _ },
				{ _, _, _, _ },
				{ _, o, _, _ },
				{ o, _, _, x }
			},
			
		};
		return box;*/
	}
}
