
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tictactoe.game.Computer;
import tictactoe.game.Human;
import tictactoe.game.Player;
import tictactoe.game.model.Box;
import tictactoe.game.model.Cube;

public class CubeTest {
	private final Box _ = new Box(Player.EMPTY_MARK);
	private final Box x = new Box(Human.MARK);
	private final Box o = new Box(Computer.MARK);

	private Cube cube;

	@Test
	public void testHorizontalLine() {
		Box[][][] box = {
		// Z=0
		{ 
			{ x,_, _, _ },
			{ x, _, _, _ },
			{ x, _, _, _ },
			{ x, _, _, _ }
		},
		//Z=1
		{
			{ _, _, _, _ },
			{ _, _, _, _ },
			{ _, _, _, _ },
			{ _, _, _, _ }
		},
		//Z=2
		{
			{ _, _, _, _ },
			{ _, _, _, _ },
			{ _, _, _, _ },
			{ _, _, _, _ }
		},
		//Z=3
		{
			{ _, _, _, _ },
			{ _, _, _, _ },
			{ _, _, _, _ },
			{ _, _, _, _ }
		},		
	  };


		cube = new Cube(box, box[0][0].length);
		assertEquals(0, cube.evaluate());
	}
	
	//@Test
	public void testVerticalLine() {
		Box[][][] box = {
				// Z=0
				{ 
					{ x, o, _ },
					{ _, o, _ },
					{ _, o, _ }
				},
				//Z=1
				{
					{ _, _, _ },
					{ _, _, _ },
					{ _, _, _ }
				},
				//Z=2
				{
					{ _, _, _ },
					{ _, _, _ },
					{ _, _, _ }
				}
		};
		cube = new Cube(box, box[0][0].length);
		assertEquals(-1000, cube.evaluate());
	}

	//@Test
	public void testDiagonalLine() {
		Box[][][] box = {
				// Z=0
				{ 
					{ x, o, o },
					{ _, o, _ },
					{ _, _, _ }
				},
				//Z=1
				{
					{ _, _, _ },
					{ _, o, _ },
					{ x, _, _ }
				},
				//Z=2
				{
					{ _, _, _ },
					{ _, _, _ },
					{ o, _, x }
				}
		};

		cube = new Cube(box, box[0][0].length);
		assertEquals(-1000, cube.evaluate());
	}
	
}
